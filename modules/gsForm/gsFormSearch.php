<?php
    define( "GSFORM_DBHOST", "localhost" );
    define( "GSFORM_DBNAME", "eu_twix" );
    define( "GSFORM_DBUSER", "root" );
    define( "GSFORM_DBPASSWORD", "BigCheef3137" );

    $link = @mysqli_connect( GSFORM_DBHOST, GSFORM_DBUSER, GSFORM_DBPASSWORD, GSFORM_DBNAME );
    if( !$link )
    {
        returnError( "could not connect do DB" );
    }
    mysqli_query($link, 'SET CHARACTER SET utf8');

    /*********************
    ******* INIT FIELDS **
    *********************/

    $table = $_GET["table"];
    $searchBy = array();

    $nameFields = array();
    $namePattern = empty( $_GET["namePattern"] ) ? null : $_GET["namePattern"];
    $valueField = empty( $_GET["valueField"] ) ? "id" : $_GET["valueField"];
    $terms = splitBy( " ", $_REQUEST["terms"] );
    $rows = array();
    $defaultValue = $_GET["defaultValue"];

    if( empty( $terms ) && empty( $defaultValue ) )
    {
        returnResponse( $rows );
    }

    if( !empty( $_GET["searchBy"] ) )
    {
        $searchBy = splitBy( ",", $_GET["searchBy"] );
    }
    else
    {
        $searchBy[] = "name";
    }

    if( !empty( $_GET["nameFields"] ) )
    {
        $nameFields = splitBy( ",", $_GET["nameFields"] );
    }
    else
    {
        $nameFields = $searchBy;
    }

    if( empty( $namePattern ) )
    {
        $namePattern = implode( " ", array_fill( 0, count( $nameFields ), "%s" ) );
        // for 3 display fields it will be: %s %s %s
    }

    /*********************
    ******* QUERY ********
    *********************/

    if( empty( $defaultValue ) )
    {
        $queryParts = array();
        foreach( $terms AS $term )
        {
            $subQueryParts = array();
            foreach( $searchBy AS $field )
            {
                $subQueryParts[] = "LOWER(`$field`) LIKE '%$term%'";
            }

            $queryParts[] = "(" . implode( " OR ", $subQueryParts ) . ")";
        }

        if( $queryParts )
        {
            $where = "(" . implode( " AND ", $queryParts ) . ")";
        }
        else
        {
            $where = " 1 = 0 ";
        }
    }
    else
    {
        $where = "`$valueField` = '$defaultValue'";
    }


    $q = "
        SELECT
        *
        FROM $table
        WHERE $where
    ";

    try
    {
        $result = mysqli_query( $link, $q );
    }
    catch( Exception $e )
    {
        returnError( "query error" );
    }

    /*********************
    ******* RESPONSE *****
    *********************/

    $response = array();
    while( $row = mysqli_fetch_assoc( $result ) )
    {
        $responseRow = array( "data" => $row );

        if( empty( $row[$valueField] ) )
        {
            returnError( sprintf("Value field %s is empty for row %s", $valueField, print_r( $row, true ) ) );
        }

        $responseRow["value"] = $row[$valueField];
        $displayValues = array();

        foreach( $nameFields AS $displayField )
        {
            $displayValues[] = $row[$displayField];
        }

        $responseRow["name"] = vsprintf( $namePattern, $displayValues );

        $response[] = $responseRow;
    }

    returnResponse( $response );

    /*********************
    ******* HELPERS ******
    *********************/
    function splitBy( $splitter, $string )
    {
        $items = explode( $splitter, $string );
        $result = array();

        foreach( $items AS $key => $item )
        {
            $item = trim( $item );
            if( !empty( $item ) )
            {
                $result[] = strtolower( $item );
            }
        }

        return $result;
    }

    function returnError( $error )
    {
        returnResponse( array(
            "error" => $error
        ) );
    }

    function returnResponse( $response )
    {
        header( "Content-Type: application/json" );
        echo json_encode( $response );
        exit();
    }


    mysqli_close( $link );
?>
