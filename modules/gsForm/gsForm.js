/* v. 1.0.2 */

$(document).click(function(event) {
    // IF NOT CLICK IN THE COMBOBOX
    if(!$(event.target).closest('.gsform-combobox-open').length)
    {
        // IF WE HAVE AN OPEN COMBOBOX
        if($('.gsform-combobox-open').length)
        {
            $('.gsform-combobox-open').trigger("combobox:close");
        }
    }
});

(function ($) {
    var timeout = 800;
    var spanWidth = 36;
    $.fn.gsForm = function (options) {

        return this.each(function() {

            if( options == "resize" )
            {
                resize(this);
                return this;
            }

            this.gsForm = {};
            this.gsForm.settings = $.extend({
                "disable_submit": false
            }, options);

            if($(this).data("validate")) {
                this.gsForm.settings.validate = $(this).data("validate");
            }

            if( $(this).find(".recaptcha").length )
            {
                var self = this;
                var div = $(this).find(".recaptcha");
                var cOptions = {
                    sitekey: div.data("sitekey"),
                    callback: function(){
                        div.find("textarea").change();
                    }
                }

                if( div.data("language") )
                {
                    cOptions.hl = div.data("language");
                }

                grecaptcha.render($(".recaptcha").get(0), cOptions);
                div.find("textarea").data("required", 1).data("captcha", div.data("validate"))
                div.append($("<div></div>").addClass('captcha-error').html($(".recaptcha").data("error-message") || "This field is required" ) );
            }

            init_fields(this);

            obj = this;

            if( $(this).find("input[type=submit]").data("disableSubmit") )
            {
                this.gsForm.settings.disable_submit = true;
            }

            $(this).on('submit', function(event, force_submit) {
                var r = validate_all(this, {"is_submit": true});

                if(!r)
                {
                    var submitData = $(this).find('input[type=submit]').data();

                    var message = submitData.errorMessage || 'You still have invalid fields in your form';
                    var methodName = submitData.errorMethod || 'alert';

                    eval(methodName + '("' + message + '");');

                    event.preventDefault();
                    return false;
                }

                //console.log('SUBMIT!!!');
                //event.preventDefault();
                //return false;
            } );

            validate_all($(this), {quiet: true});

            return this;
        });
    };

    function init_fields(obj)
    {
        $(obj).find("input[type=submit]").each(function(){init_input_submit(this);});
        $(obj).find("input[type=text]").each(function(){init_input_text(this);});
        $(obj).find("input[type=password]").each(function(){init_input_text(this);});
        $(obj).find("input[type=file]").each(function(){init_input_file(this);});
        $(obj).find("textarea").each(function(){init_input_text(this);});
        $(obj).find("select").each(function(){init_input_select(this);});
    }

    function init_input_submit(input) {
        var loading = $("<div></div>")
            .css({
                width: $(input).outerWidth(),
                height: $(input).outerHeight(),
                margin: $(input).css('margin'),
                'text-align': 'center'
            })
            .addClass('loading-submit');

        $(input).after(loading);
    }

    function init_input_general_appearance(input)
    {
        var container = $(input).parent().closest("div");

        $(input).addClass("gsform-general-appearance");

        if( $(input).prop("tagName").toLowerCase() == "textarea" )
            $(input).addClass("gsform-general-appearance-textarea");

        if( container.find(">label").length )
        {
            container.addClass("gsform-container-with-label" );
        }

        container.addClass("gsform-field-container");
    }

    function combobox_static_search( container, val )
    {
        var results = new Array();
        var data    = $(container).data();
        var words   = val.trim().split(/\s+/);
        if( words.length )
        {
            $.each( data.db, function( i, row ){
                var text = row.name.toLowerCase();
                var count = 0;

                for(j = 0; j < words.length; j++)
                {
                    var word = words[j].toLowerCase();

                    if( text.indexOf(word) != -1 )
                    {
                        count++;
                    }
                }

                if( count == words.length )
                {
                    row.gsFormCount = count;
                    results.push( row );
                }
            });
        }

        if( words.length && results.length )
            $(container).trigger("database:resultsReceived", [results]);
        else
            $(container).trigger("database:noResult", ["no result"]);
    }

    function combobox_ajax_search( container, val, force, defaultValue )
    {
        var results = new Array();
        var data    = $(container).find("select").data();

        if( data.timeout )
        {
            clearTimeout( data.timeout );
        }

        $(container).trigger("database:noResult", ["searching ..."]);
        if( !force )
        {
            timeout = setTimeout( function(){
                combobox_ajax_search( container, val, true )
            }, 300 );
            $(container).find("select").data("timeout", timeout);

            return;
        }

        var url = data.comboboxAjax;
        var terms = val;
        var table = data.comboboxAjaxTable || alert("Please set the ajax table");
        var searchBy = data.comboboxAjaxSearchBy || "name";
        var nameFields = data.comboboxAjaxNameFields || "";
        var namePattern = data.comboboxAjaxNamePattern || "";
        var valueField = data.comboboxAjaxValueField || "id";

        var params = {
            terms: terms,
            table: table,
            defaultValue: defaultValue || null,
            searchBy: searchBy,
            nameFields: nameFields,
            namePattern: namePattern,
            valueField: valueField
        }

        $.getJSON( url, params, function( response ) {
            if( response.error )
            {
                alert(response.error);
            }
            else
            {
                if( response.length )
                {
                    if( defaultValue )
                    {
                        response[0].initialResponse = true;
                        $(container).trigger("database:resultSelected", response);
                    }
                    else
                    {
                        $(container).trigger("database:resultsReceived", [response]);
                    }
                }
                else
                {
                    $(container).trigger("database:noResult", ["no result"]);
                }
            }
        });
    }

    function init_input_general_appearance_combobox(input)
    {
        var container = $(input).parent().closest("div");
        var initialRow = null;

        $(input).css({
            position: "absolute",
            "z-index": "-1",
            "width": "1px"
        });

        if( container.find(">label").length )
        {
            container.addClass("gsform-container-with-label" );
        }

        /********************
        ***** INIT DATABASE *
        ********************/

        var database = new Array();
        if( $(input).val() )
        {
            initialRow = {
                "value" : $(input).val(),
                "name" : $(input).find("option:selected").html()
            };
        }

        $.each( $(input).find( "option" ), function( key, value ){
            if( !$(value).attr("value") ){
                return true;
            }
            database.push({
                "name": $(value).html(),
                "value": $(value).attr("value") || $(value).html()
            });
        });
        $(input).empty().append("<option></option>");

        /**********************
        *** ELEMENTS **********
        **********************/

        var placeholder = $("<span></span>")
            .addClass("gsform-general-appearance")
            .addClass("gsform-combobox-placeholder")
            .click(function(){
                container.trigger("combobox:toggle");
            });

        var search = $("<input />")
            .attr("type", "text")
            .attr("placeholder", "type to search")
            .data("ignore", true)
            .bind( "keyup mouseup", function(event){
                switch(event.which) {
                    case 38: // up
                        container.trigger("combobox:selectNextItem", [-1]);
                        event.preventDefault();
                        event.stopPropagation();
                        return;
                    case 40: // down
                        container.trigger("combobox:selectNextItem", [1]);
                        event.preventDefault();
                        event.stopPropagation();
                        return;
                }

                container.trigger( "database:search", [$(this).val()])
            })
            .bind( "keydown", function( event ){
                if( event.which == 13 )
                {
                    container.find("li.selected").click();
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
            });

        placeholder.html($(input).attr("placeholder") ? $(input).attr("placeholder") : "Select " + $(input).attr("name"));

        var list = $("<ul></ul>");




        container.data( "db", database )
        .data( "dbType", $(input).data("combobox-ajax") || "static")
        .on( "combobox:selectItem", function( event, index ){
            container.find("li.row").removeClass("selected");
            container.find("li.row:eq(" + index + ")").addClass("selected");

            var totalHeight = 0;
            var elementHeight = parseInt( container.find("li.row:eq(" + index + ")").height() );
            var ulHeight = container.find( "ul" ).height();
            var scrollTop = container.find( "ul" ).scrollTop();
            for( i = 0; i < index; i++ )
            {
                totalHeight += parseInt( container.find("li.row:eq(" + i + ")").height() );
            }

            if( scrollTop > totalHeight /* too low */ || scrollTop < totalHeight + elementHeight - ulHeight )
            {
                container.find( "ul" ).scrollTop( totalHeight );
            }

            container.data("selectedItem", index);
        })
        .on( "combobox:selectNextItem", function(event, direction){
            var current = container.data("selectedItem");
            var total = container.find("li.row").length;

            if( total == 1 ) return;

            var diff = direction > 0 ? 1 : -1;
            var nextIndex = current + diff;

            if( nextIndex < 0 )
            {
                nextIndex = total - 1;
            }

            if( nextIndex > total - 1 )
            {
                nextIndex = 0;
            }

            container.trigger("combobox:selectItem", [nextIndex]);
        })
        .on( "database:search", function(event, val){
            var data = $(this).data();
            if( data.dbType == "static" )
            {
                if( val )
                {
                    combobox_static_search( container, val );
                }
                // else
                // {
                //     container.find("ul").empty();
                // }
            }
            else
            {
                if( val )
                {
                    combobox_ajax_search( container, val );
                }
                else
                {
                    container.find("ul").empty();
                }
            }
        })
        .on( "database:resultsReceived", function(event, results){
            var self = $(this);
            var ul = self.find("ul");
            $(ul).empty();
            $.each( results, function( i, row ){
                var li = $("<li></li>").addClass("row").html( row.name ).click(function(){
                    self.trigger("database:resultSelected", [row]);
                });

                ul.append(li);
            });
            if( $(ul).find("li.row").length )
            {
                container.trigger("combobox:selectItem", [0])
                container.data("selectedItem", 0);
            }
        })
        .on( "database:noResult", function(event, message){
            var self = $(this);
            var ul = self.find("ul");
            $(ul).empty();
            var li = $("<li></li>").addClass("no-result").html( message );
            ul.append(li);
        })
        .on( "database:resultSelected", function( event, row ){
            placeholder.html( row.name );
            container.trigger("combobox:close");
            $(input).append(
                $("<option></option>").html( row.name ).val( row.value )
            ).val( row.value ).trigger("combobox:changed");

            if( !row.initialResponse && row.value ) $(input).change();
        })
        .on( "combobox:toggle", function(event){
            if( container.hasClass("gsform-combobox-open") )
            {
                container.trigger("combobox:close");
            }
            else
            {
                container.trigger("combobox:open");
            }
        })
        .on( "combobox:open", function(event){
            container.addClass("gsform-combobox-open");
            search.val("").focus();
            list.empty();

            var data = $(this).data();
            if( data.dbType == "static" )
            {
                combobox_static_search( container, "" );
            }
        })
        .on( "combobox:close", function(event){
            $(container).removeClass("gsform-combobox-open");
        });

        if( initialRow )
        {
            initialRow.initialResponse = true;
            container.trigger("database:resultSelected", [initialRow] );
        }
        else
        {
            if( container.data("dbType") != "static" )
            {
                if( $(input).data("combobox-ajax-default") )
                {
                    container.trigger( "database:resultSelected", [{name: "loading...", value: 0}] );
                    combobox_ajax_search( container, null, true, $(input).data("combobox-ajax-default") );
                }
            }
        }

        var combobox = $("<div></div>")
            .addClass("gsform-combobox")
            .attr("name", $(input).attr("name") );

        combobox.append( search ).append( list );

        container.append(placeholder).append( combobox );

        container.addClass("gsform-field-container").addClass("gsform-combobox-container");
    }

    function init_input_general_appearance_file(input)
    {
        var container = $(input).parent().closest("div");

        $(input).addClass("gsform-general-appearance").hide(); // SOUND STUPID BUT SPAN SCALES TO THE INPUT, EVEN IF HIDDEN :)
        container.addClass("gsform-field-container");

        var placeholder = $("<span></span>")
            .addClass("gsform-general-appearance")
            .addClass("gsform-file-placeholder")
            .click(function(){
                $(input).click();
            });

        var text = "";

        if( $(input).val() == "" )
        {
            text = $(input).attr("placeholder") ? $(input).attr("placeholder") : "Click to select " + $(input).attr("name");
        }
        else
        {
            text = $(input).attr("placeholder_selected") ? $(input).attr("placeholder_selected") : "Selected " + $(input).attr("name") + ":";
            text = text + " " + $(input).val();
        }

        placeholder.html(text);
        container.append(placeholder);

        if( container.find(">label").length )
        {
            container.addClass("gsform-container-with-label" );
            placeholder.css({
                top: parseInt(container.css("padding-top")) + container.find('label').height()
            });
        }

        $(input).change(function(){
            var text = $(input).attr("placeholder_selected") ? $(input).attr("placeholder_selected") : "Selected " + $(input).attr("name") + ":";
            text = text + " " + $(input).val();
            $(input).parent().closest("div").find(".gsform-file-placeholder").html( text );
        });
    }

    function init_input_placeholder(input)
    {
        var placeholder = $(input).attr("placeholder");
        if( !placeholder )
        {
            placeholder = $(input).attr("name").replace(/_/, " ");
        }

        $(input)
            .attr("placeholder", placeholder)
            .blur(function(){ $(input).attr("placeholder", placeholder); })
            .click(function(){ $(input).attr("placeholder", "" ); });
    }

    function init_input_error_message(input)
    {
        var data = $(input).data();

        // ADD ERROR MESSAGE
        var p = $("<p></p>")
            .addClass("gsform-general-appearance")
            .addClass("gsform-error-message")
            .html(data.errorMessage);

         $(input).parent().closest("div").append(p);

        var p = $("<p></p>")
            .addClass("gsform-general-appearance")
            .addClass("gsform-warning-message")
            .html(data.errorMessage);

        $(input).parent().closest("div").append(p);
    }


    function init_input_status_span(input)
    {
        var data = $(input).data();
        var container = $(input).parent().closest("div");

        var span = $("<span></span>")
            .addClass("gsform-status")
            .addClass("gsform-general-appearance")
            .html("");

        if( data.required )
        {
            span.html("&nbsp;").addClass("gsform-status-required-default");
        }
        else
        {
            span.html("&nbsp;");
        }

        $(input).after(span);

        if(container.find('label').length) {
            span.css({
                top: parseInt(container.css("padding-top")) + container.find('label').height()
            });
        }
    }

    function init_input_validation(input)
    {
        var data = $(input).data();

        if( data.duplicateOf )
        {
            $(input).parent().closest("form").find( "input[name=" + data.duplicateOf + "]" ).data("duplicateTo", $(input).attr("name") );
        }

        $(input).bind( "focus", function(){ $(input).parent().closest("div").addClass("focus") });
        $(input).bind( "blur", function(){ $(input).parent().closest("div").removeClass("focus") });

        input.gsForm = {};
        if( $(input).prop("tagName").toLowerCase() == "select" || $(input).attr("type") == "file" )
        {
            $(input).bind("combobox:changed", function(){
                validate_input(input, { validate_now: true })
                validate_all($(input).parent().closest("form"), { quiet: true });
            });
        }
        else
        {
            $(input).bind("blur change", function(){
                validate_input(input, { validate_now: true })
                if( !$(input).data("captcha") )
                    validate_all($(input).parent().closest("form"), { quiet: true });
            });

            $(input).bind("keyup", function(){
                validate_input(input);
            });

            $(input).bind("keyup mouseup", function(){
                $(input).get(0).gsForm.is_valid = false;
                if( $(input).val() )
                {
                    validate_input(input);
                }
                else
                {
                    validate_input(input, { validate_now: true, quiet: true });
                }
            });
        }
    }

    function init_input_text(input)
    {
        if( $(input).data('ignore') || !$(input).attr("name") ) return;

        if( $(input).data('gs-form-initiated') )  return;
        $(input).data('gs-form-initiated', true);

        if( !$(input).hasClass("g-recaptcha-response") )
        {
            init_input_general_appearance(input);
            init_input_placeholder(input);
            init_input_error_message(input);
            init_input_status_span(input);
        }

        init_input_validation(input);
    }

    function init_input_select(input)
    {
        if( $(input).data('ignore') ) return;

        if( $(input).data('gs-form-initiated') )  return;

        if( !$(input).data('combobox') )
        {
            $(input).closest("div").addClass("gsform-field-container-select");
            return init_input_text( input );
        }

        // IS COMBOBOX
        // IS COMBOBOX

        $(input).data('gs-form-initiated', true);

        init_input_general_appearance_combobox(input);
        init_input_placeholder(input);
        init_input_error_message(input);
        init_input_status_span(input);
        init_input_validation(input);
    }

    function init_input_file(input)
    {
        if( $(input).data('gs-form-initiated') )  return;
        $(input).data('gs-form-initiated', true);

        init_input_general_appearance_file(input);
        init_input_placeholder(input);
        init_input_error_message(input);
        init_input_status_span(input);
        init_input_validation(input);
    }

    function validate_input( input, custom_options )
    {
        if( $(input).data('ignore') || !$(input).attr("name") ) return true;

        var options =  $.extend({
                "validate_now": false
            }, custom_options);
        // ALLOW `var timeout` SECONDS TO ADD ANOTHER CHAR

        if( input.gsForm.timer )
        {
            clearTimeout(input.gsForm.timer);
        }

        if( !options.validate_now )
        {
            input.gsForm.timer = setTimeout(function(){
                    validate_input(input, { validate_now: true });
                    validate_all($(input).parent().closest("form"), { quiet: true });
                }, timeout);
            return true;
        }

        if( $(input).attr("disabled") )
        {
            mark_as_success(input, options.quiet);
            return true;
        }

        // ACTUAL VALIDATION
        var value = $(input).val();
        var data = $(input).data();
        var name = $(input).attr("name");
        if( options.quiet )
        {
            if( typeof(input.gsForm.is_valid) != "undefined" )
            {
                return input.gsForm.is_valid;
            }
            else
            {
                if( value.length )
                {
                    options.quiet = false;
                }
            }
        }

        if( data.duplicateTo )
        {
            validate_input( $(input).parent().closest("form").find( "input[name=" + data.duplicateTo + "]" ), { validate_now: true, quiet: options.quiet } );
        }

        if( data.required )
        {
            if( value == "" )
            {
                if( data.captcha )
                {
                    if( !options.quiet )
                        $(input).parent().closest(".recaptcha").find(".captcha-error").show();
                }
                else
                {
                    mark_as_required(input, options.quiet);
                }

                return false;
            }
            else
            {
                $(input).parent().closest(".recaptcha").find(".captcha-error").hide();
                remove_mark_as_required(input, options.quiet);
            }
        }

        if( data.minChars )
        {
            if( value.length < data.minChars )
            {
                mark_as_error(input, options.quiet);
                return false;
            }
            else
            {
                remove_mark_as_error(input, options.quiet);
            }
        }

        if( data.maxChars )
        {
            if( value.length > data.maxChars )
            {
                mark_as_error(input, options.quiet);
                return false;
            }
            else
            {
                remove_mark_as_error(input, options.quiet);
            }
        }

        if( data.email )
        {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if( !regex.test(value) && value != "" )
            {
                mark_as_error(input, options.quiet);
                return false;
            }
            else
            {
                remove_mark_as_error(input, options.quiet);
            }
        }

        if( data.website )
        {
            var regex = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
            if( !regex.test(value) && value != "" )
            {
                mark_as_error(input, options.quiet);
                return false;
            }
            else
            {
                remove_mark_as_error(input, options.quiet);
            }
        }

        if( data.duplicateOf )
        {
            if( $(input).parent().closest("form").find( "input[name=" + data.duplicateOf + "]" ).val() != value )
            {
                mark_as_error(input, options.quiet);
                return false;
            }
            else
            {
                remove_mark_as_error(input, options.quiet);
            }
        }

        if(data.ajax)
        {
            //console.log(input.gsForm.is_valid);
        }

        if( data.captcha )
        {
            if( !input.gsForm.is_valid && !options.quiet )
            {
                $.post(data.captcha, {"value": value}, function(response) {
                    input.gsForm.is_valid = response.success;
                    validate_all($(input).parent().closest("form"), { quiet: true, validate_now: true });
                });

                return false;
            }
            else
            {
                return true;
            }

        }
        if( data.captcha && input.gsForm.is_valid )
        {
            return true;
        }

        if( data.ajax && !options.is_submit && !input.gsForm.is_valid )
        {
            $(input).closest('form').find('input[type=submit]').hide();
            $(input).closest('form').find('.loading-submit').show();

            mark_as_loading(input, options.quiet);
            $.getJSON(data.ajax, {"name": $(input).attr("name"), "value": value}, function(response){
                $(input).closest('form').find('input[type=submit]').show();
                $(input).closest('form').find('.loading-submit').hide();
                if( typeof(response.value) != "undefined" )
                {
                    $(input).off("change").val(response.value).on("change");
                }

                if( response.success )
                {
                    if( !response.warning )
                        mark_as_success(input);
                    else
                        mark_as_warning(input, false, response.warning);
                }
                else
                {
                    mark_as_error(input, false, response.error);
                }

                validate_all($(input).parent().closest("form"), { quiet: true });
            });

            return false;
        }
        else
        {
            if( data.ajax ) {
                return input.gsForm.is_valid;
            }
        }

        if( typeof(data.duplicateOf) == "undefined" )
        {
            input.gsForm.is_valid = true;

            if( value.length )
            {
                mark_as_success(input, options.quiet);
            }
            else
            {
                remove_mark_as_success(input, options.quiet);
            }
        }
        else
        {
            if( $(input).parent().closest("form").find( "input[name=" + data.duplicateOf + "]" ).get(0).gsForm.is_valid && typeof(data.ajax) == "undefined" )
            {
                input.gsForm.is_valid = true;
                mark_as_success(input, options.quiet);
            }
            else
            {
                remove_mark_as_success( input, options.quiet );
            }
        }

        return input.gsForm.is_valid;
    }

    function validate_all(form, custom_options)
    {
        var options =  $.extend({
                "validate_now": true
            }, custom_options);

        var all_valid = true;
        $(form).find("input[type=text],input[type=password],input[type=file],textarea,select").each( function(key, input){
            if( !validate_input(input, options) )
            {
                all_valid = false;
            }
        });

        if($(form).get(0).gsForm.settings["validate"]) {
            var method = $(form).get(0).gsForm.settings["validate"];

            if( !window[method](custom_options) ) {
                all_valid = false
            }
        }

        if( $(form).get(0).gsForm.settings["disable_submit"] )
            $(form).find("input[type=submit]").attr("disabled", $(form).get(0).gsForm.settings["disable_submit"] && !all_valid);

        return all_valid;
    }

    function mark_as_error(input, quiet, error_message)
    {
        input.gsForm.is_valid = false;
        remove_mark_as_success(input, quiet);
        remove_mark_as_loading(input, quiet);
        remove_mark_as_warning(input, quiet);

        var data = $(input).data();

        if( data.callback && window[data.callback] )
        {
            eval(data.callback + "(input, 'error')");
        }

        if( error_message || data.errorMessage )
        {
            var container = $(input).parent().closest("div");
            container.addClass("gsform-error-message");
            container.find("p.gsform-error-message").html(error_message || data.errorMessage);
        }

        if(!quiet)
            $(input).parent().closest("div").find("span.gsform-status").addClass("gsform-status-error");
    }

    function remove_mark_as_error(input, quiet)
    {
        $(input).parent().closest("div").removeClass("gsform-error-message").find("span.gsform-status").removeClass("gsform-status-error");
    }

    function mark_as_warning(input, quiet, error_message)
    {
        input.gsForm.is_valid = true;
        remove_mark_as_success(input, quiet);
        remove_mark_as_loading(input, quiet);

        var data = $(input).data();

        if( data.callback && window[data.callback] )
        {
            eval(data.callback + "(input, 'warning')");
        }

        var container = $(input).parent().closest("div");
        container.addClass("gsform-warning-message");
        container.find("p.gsform-warning-message").html(error_message);

        if(!quiet)
            $(input).parent().closest("div").find("span.gsform-status").addClass("gsform-status-warning");
    }

    function remove_mark_as_warning(input, quiet)
    {
        $(input).parent().closest("div").removeClass("gsform-warning-message").find("span.gsform-status").removeClass("gsform-status-warning");
    }

    function mark_as_required(input, quiet)
    {
        var data = $(input).data();
        if( data.callback && window[data.callback] )
        {
            eval(data.callback + "(input, 'required')");
        }

        remove_mark_as_success(input); remove_mark_as_error(input, quiet);
        input.gsForm.is_valid = false;

        if(!quiet)
        {
            $(input).parent().closest("div").find("span.gsform-status").addClass("gsform-status-required");

            if( data.requiredMessage )
            {
                var container = $(input).parent().closest("div");
                container.addClass("gsform-warning-message");
                container.find("p.gsform-warning-message").html(data.requiredMessage);
            }
        }
    }

    function remove_mark_as_required(input, quiet)
    {
        $(input).parent().closest("div").find("span.gsform-status").removeClass("gsform-status-required");
    }

    function mark_as_success(input, quiet)
    {
        var data = $(input).data();
        if( data.callback && window[data.callback] )
        {
            eval(data.callback + "(input, 'success')");
        }

        remove_mark_as_loading(input, quiet);
        remove_mark_as_warning(input, quiet);
        remove_mark_as_error(input, quiet);
        input.gsForm.is_valid = true;

        if(!quiet)
            $(input).parent().closest("div").find("span.gsform-status").addClass("gsform-status-success");
    }

    function remove_mark_as_success(input, quiet)
    {
        $(input).parent().closest("div").find("span.gsform-status").removeClass("gsform-status-success");
    }

    function mark_as_loading(input, quiet)
    {
        remove_mark_as_success(input, quiet);
        remove_mark_as_error(input, quiet);
        var data = $(input).data();
        if( data.callback && window[data.callback] )
        {
            eval(data.callback + "(input, 'loading')");
        }

        input.gsForm.is_valid = false;
        if(!quiet)
            $(input).parent().closest("div").find("span.gsform-status").addClass("gsform-status-loading");
    }

    function remove_mark_as_loading(input, quiet)
    {
        $(input).parent().closest("div").find("span.gsform-status").removeClass("gsform-status-loading");
    }


    function init_input_password(input)
    {
        var container = $(input).parent().closest("div");
        var data = $(input).data();
        var span = $("<span></span>").addClass("gsFormStatus").html("status");
        container.append(span);
    }

    function init_textarea(input)
    {
        var container = $(input).parent().closest("div");
        var span = $("<span></span>").addClass("gsFormStatus").html("status");
        container.append(span);
    }

    function show(obj, index)
    {
        images = obj.images;
        for( i = 0; i < images.length; i++ )
        {
            $(images[0]).css("z-index", -1);
        }

        $(images[index]).css("z-index", obj.settings["starting_z_index"]).css("opacity", "1");                // first image on top
        $(images[nextIndex(obj, index)]).css("z-index", obj.settings["starting_z_index"] - 1);      // second image below first

        setTimeout(function(){ animate(obj, index); }, obj.settings["frame_duration"]);
    }

    function animate(obj, index)
    {
        images = obj.images;

        $(images[index]).animate({"opacity": 0}, obj.settings["animation_duration"]);
        $(images[nextIndex(obj, index)]).animate({"opacity": 1}, obj.settings["animation_duration"], function(){
            show(obj, nextIndex(obj, index));
        });
    }

    function nextIndex(obj, index)
    {
        return ( index + 1 ) % obj.images.length;
    }

    function isScrolledIntoView( element ) {
        var elementTop    = element.getBoundingClientRect().top,
            elementBottom = element.getBoundingClientRect().bottom;

        return elementTop >= 0 && elementBottom <= window.innerHeight;
    }

}( jQuery ));


$(window).load(function() {
    $(".gsForm").gsForm();
});
