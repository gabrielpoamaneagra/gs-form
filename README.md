Javascript Validation

v 1.0.1
 - data-email does not imply data-required
 - disabled inputs evaluates as true

v 1.0.2
 - [ADDED] data-ignore; skips the e-mail from validation
 - [BUG] checked on empty

v.1.0.3
 - [ADDED] combobox


v.1.0.4
 - [ADDED] captcha
 - [ADDED] labels